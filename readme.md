## 仓库简介

华为云技术服务团队基于SaaS项目技术支持实践，沉淀了SaaS应用开发相关套件，旨在为企业级开发者提供SaaS应用改造和技术构建升级提供技术参考，包括微服务架构、多租隔离设计、多租户路由、数据存储多租设计、数据源管理等。更多SaaS相关技术细节可参考：华为云开发者文档中心[SaaS应用开发指导](https://support.developer.huaweicloud.com/doc/zh-cn_topic_0000001271256348-0000001271256348)。

## 项目总览

| 项目   | 介绍                         | 仓库                                                         |
| ------ | ---------------------------- | ------------------------------------------------------------ |
| 开发指南| SaaS应用开发指导             | [huaweicloud-SaaS-development-guide](https://gitee.com/HuaweiCloudDeveloper/huaweicloud-saas-developer-guide) |
|示例1  | SaaS应用示例项目         | [saas-housekeeper](https://gitee.com/HuaweiCloudDeveloper/saas-housekeeper) |
| 示例2 | SaaS应用示例项目       | [huaweicloud-saas-landing-sample](https://gitee.com/HuaweiCloudDeveloper/huaweicloud-saaslanding-sample) |
| 路由插件| SaaS路由插件                 | [saas-tenant-router-starter](https://gitee.com/HuaweiCloudDeveloper/saas-tenant-router-starter) |
| 分析平台 | 基于Hive搭建离线数仓分析平台 | [huaweicloud-analytics-reference-architecture](https://gitee.com/HuaweiCloudDeveloper/huaweicloud-analytics-reference-architecture) |
| 日志平台   | 基于LTS搭建日志分析系统      | [huaweicloud-LTS-SDK-intergration-sample](https://gitee.com/HuaweiCloudDeveloper/huaweicloud-lts-sdk-intergration-sample) |
| Flyway | 数据库迁移工具               | [flyway](https://gitee.com/HuaweiCloudDeveloper/flyway)      |
| 服务网格| 基于CCE部署Istio官方Demo     | [huaweicloud-istio-bookinfo-development-guide](https://gitee.com/HuaweiCloudDeveloper/huaweicloud-istio-bookinfo-development-guide) |
| 服务网格| SaaS-housekeeper istio改造实践     | [huaweicloud-istio-k8s-saas-housekeeper](https://gitee.com/HuaweiCloudDeveloper/huaweicloud-istio-k8s-saas-housekeeper) |
| saas赋能课程| DTSE Tech Talk     | [培训课程和实践](https://bbs.huaweicloud.com/blogs/364602?utm_source=push&utm_campaign=developer&utm_content=content) |